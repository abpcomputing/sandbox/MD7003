# %%
import json                                    
import numpy as np                          
import pandas as pd
import matplotlib.pyplot as plt

#https://github.com/PyCOMPLETE/FillingPatterns       
import fillingpatterns as fp                            
import WireDAQ.SigmaEff as sEff


INJscheme_1 = {}
INJscheme_1['beam1'] = pd.DataFrame({   'RFBucket':[121,  10671,   19611,   28551],
                                        'Length'  :[12,     48,     48,        48]})

INJscheme_1['beam2'] = pd.DataFrame({   'RFBucket':[1  ,  10671,   19611,   28551],
                                        'Length'  :[12,     48,     48,        48]})

INJscheme_2 = {}
INJscheme_2['beam1'] = pd.DataFrame({   'RFBucket':[121,  18251,   20641,   23031,  25421],
                                        'Length'  :[12,     36,     36,        36,      36]})

INJscheme_2['beam2'] = pd.DataFrame({   'RFBucket':[1  ,  9311,   11701,   14091,  16481],
                                        'Length'  :[12,     36,     36,        36,      36]})

INJscheme_3 = {}
INJscheme_3['beam1'] = pd.DataFrame({   'RFBucket':[121,  7261,   15981,   25121],
                                        'Length'  :[12,     1,     48,        1]})

INJscheme_3['beam2'] = pd.DataFrame({   'RFBucket':[1  ,  7261,   15981,   25121],
                                        'Length'  :[12,     1,     48,        1]}) 

# %%

for scheme,INJscheme in zip([   '25ns_156b_144_90_96_48bpi_4inj_MD7003',
                                '25ns_732b_31_12_720_180bpi_5inj_MD7003',
                                '25ns_62b_50_2_2_48bpi_4inj_MD7003'],
                            [INJscheme_1, INJscheme_2, INJscheme_3]):
    




    # Make filling from INJScheme
    filling_scheme = {}                                                                                  
    for beam in ['beam1','beam2']:
        filling_scheme[beam] = np.zeros(3564, dtype=int)

        for bucket_s,train_l in zip(INJscheme[beam].RFBucket,INJscheme[beam].Length):
            # get bunch number
            bunch_s = bucket_s//10
            bunch_e = bunch_s + train_l

            filling_scheme[beam][bunch_s:bunch_e] = 1

        filling_scheme[beam] = filling_scheme[beam].tolist()
    #========================================================================               
                        
    with open(f'Utilities/{scheme}_filling.json','w') as fid:                        
        json.dump(filling_scheme, fid)                         


    #patt = fp.FillingPattern.from_json('MD7003_filling.json')                 
    #patt.compute_beam_beam_schedule(n_lr_per_side=21) 
    patt = sEff.getFillingPattern(f'Utilities/{scheme}_filling.json')
    # Clearing some warnings
    #clear_output(wait=False)

    # Showing relevant info
    INJscheme['beam1'].insert(0,'Beam','Beam 1')
    INJscheme['beam2'].insert(0,'Beam','Beam 2')
    print(pd.concat([INJscheme['beam1'],INJscheme['beam2']],axis=1))

    fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
    for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

        plt.sca(ax)
        plt.stem(bb_df.index,np.ones(len(bb_df.index)),markerfmt='none',basefmt='none',linefmt=color)

        plt.xlim([-15,3564])
        plt.ylim([0,1.2])
    fig.suptitle('Filling Pattern ' + scheme)
    plt.xlabel('Bunch number')
    plt.tight_layout()
    plt.savefig(f'Utilities/{scheme}_filling.png',dpi=300,format='png')


    fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
    for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

        plt.sca(ax)
        plt.plot(bb_df['# of LR in ATLAS/CMS'].values,'o',color=color)

        plt.xticks(np.arange(len(bb_df))[::4],[f"T{_bunch['Train']}, B{_bunch['Tag']}" for _idx,_bunch in bb_df.iterrows()][::4],rotation=90)
    fig.suptitle('Number of LRs in ATLAS/CMS ' + scheme)
    plt.xlabel('Train,Bunch')
    plt.tight_layout()
    plt.savefig(f'Utilities/{scheme}_LRs_IP1_5.png',dpi=300,format='png')


    fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
    for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

        plt.sca(ax)
        plt.plot(bb_df['# of LR in LHCB'].values,'o',color=color)

        plt.xticks(np.arange(len(bb_df))[::4],[f"T{_bunch['Train']}, B{_bunch['Tag']}" for _idx,_bunch in bb_df.iterrows()][::4],rotation=90)
    fig.suptitle('Number of LRs in LHCB ' + scheme)
    plt.xlabel('Train,Bunch')
    plt.tight_layout()
    plt.savefig(f'Utilities/{scheme}_LRs_IP8.png',dpi=300,format='png')

    fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
    for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

        plt.sca(ax)
        plt.plot(bb_df['# of LR in ALICE'].values,'o',color=color)

        plt.xticks(np.arange(len(bb_df))[::4],[f"T{_bunch['Train']}, B{_bunch['Tag']}" for _idx,_bunch in bb_df.iterrows()][::4],rotation=90)
    fig.suptitle('Number of LRs in ALICE ' + scheme)
    plt.xlabel('Train,Bunch')
    plt.tight_layout()
    plt.savefig(f'Utilities/{scheme}_LRs_IP2.png',dpi=300,format='png')

# %%

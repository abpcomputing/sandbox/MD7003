# %% 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
import pandas as pd
import glob
import yaml
from pathlib import Path


sys.path.append('../')


# Make sure this import is after pandas
import WireDAQ.PandasPlus

import WireDAQ.Constants as cst
import WireDAQ.NXCALS as nx
import WireDAQ.SigmaEff as sEff

import Utilities.plotter as MDplt
import Utilities.checker as MDchecks
# Matplotlib config
#============================
for key in plt.rcParams.keys():
    if 'date.auto' in key:
        plt.rcParams[key] = "%H:%M"
#============================
    

# Creating NXCALS variable containers
#============================
wires     = {'B1': [nx.NXCALSWire(loc = loc) for loc in ['L1B1','L5B1']],
             'B2': [nx.NXCALSWire(loc = loc) for loc in ['R1B2','R5B2']]}
beams     = [nx.NXCALSBeam(name) for name in ['B1','B2']]
buckets   = np.arange(3564)
#============================


with open('config.yaml','r') as fid:
    configuration = yaml.safe_load(fid)

TZONE = configuration['time_zone']




# Loading filling pattern with extra train info
patt = sEff.getFillingPattern(configuration['filling_scheme'])

# %%
if True:
    # Finding last file and importing data
    parquet_path = Path(configuration['parquet_path'])
    filePool = sorted(list(parquet_path.glob('*')))
    database = sEff.import_MD_Data(filePool[-1])

    # Taking subset defined in the config
    if configuration['stop_ts'] == 'None':
        configuration['stop_ts'] = str(database['Timestamp'].max()).split('+')[0]

    database = database[(pd.Timestamp(configuration['start_ts'],tz=TZONE)<database['Timestamp'])&
                    (pd.Timestamp(configuration['stop_ts'],tz=TZONE)>database['Timestamp'])]
    #============================


    # Analysis
    #============================



    # # # Computing intensity avg (every 20 seconds) with calibration BCT_A+BCT_B
    #BCT_avg  = sEff.compute_BCT_avg(database,configuration)
    #database = pd.concat([database,BCT_avg])
    #database = database.sort_index()

    # # Computing Lumi tot (ATLAS + CMS bunch by bunch)
    #Lumi_tot = sEff.computeLumiTot(database)
    #database = pd.concat([database,Lumi_tot])
    #database = database.sort_index()
# %%

data = ['LhcStateTracker:LHCBEAM:IP1-XING-H-MURAD:value',
 #'LhcStateTracker:LHCBEAM:IP2-XING-H-MURAD:target',
 'LhcStateTracker:LHCBEAM:IP5-XING-H-MURAD:value',
 'LhcStateTracker:LHCBEAM:IP8-XING-H-MURAD:value',

 'LhcStateTracker:LHCBEAM:IP1-XING-V-MURAD:value',
 'LhcStateTracker:LHCBEAM:IP2-XING-V-MURAD:value',
 'LhcStateTracker:LHCBEAM:IP5-XING-V-MURAD:value',
 'LhcStateTracker:LHCBEAM:IP8-XING-V-MURAD:value',

 'LhcStateTracker:LHCBEAM:IP1-SEP-H-MM:value',
 'LhcStateTracker:LHCBEAM:IP2-SEP-H-MM:value',
 'LhcStateTracker:LHCBEAM:IP5-SEP-V-MM:value',
 'LhcStateTracker:LHCBEAM:IP8-SEP-V-MM:value',
 #'LhcStateTracker:LHCBEAM:IP8-SEP-H-MM:target',

 'ATLAS:LUMI_TOT_INST',
 'ALICE:LUMI_TOT_INST',
 'CMS:LUMI_TOT_INST',
 'LHCB:LUMI_TOT_INST',
 'HX:BETASTAR_IP1',
 'HX:BETASTAR_IP2',
 'HX:BETASTAR_IP5',
 'HX:BETASTAR_IP8',
 'LHC.BQM.B1:NO_BUNCHES',
 'LHC.BQM.B2:NO_BUNCHES',


 'RPMC.UL14.RBBCW.L1B1:I_MEAS',
 'RPMC.UL16.RBBCW.R1B2:I_MEAS',
 'RPMC.UL557.RBBCW.R5B2:I_MEAS',
 'RPMC.USC55.RBBCW.L5B1:I_MEAS',

 'LHC.BCCM.B1.A:BEAM_ENERGY',
 'LHC.BCCM.B2.A:BEAM_ENERGY',

 'LHC.EXP.MAG.ALICE-DIPOLE.FIXDISPLAY.PLOT:Acquisition:I_MEAS',
 'LHC.EXP.MAG.LHCb-DIPOLE.FIXDISPLAY.PLOT:Acquisition:I_MEAS',

 'RPMBD.RR17.ROF.A12B1:I_MEAS',
 'RPMBD.RR17.ROD.A12B1:I_MEAS',
 'RPMBD.RR17.ROD.A12B2:I_MEAS',
 'RPMBD.RR17.ROF.A12B2:I_MEAS',
 ]


for ii in data:
    try:
        plt.figure()
        aux = database[['Timestamp',ii]].dropna()
        aux.index.name = None
        plt.plot( aux['Timestamp'], aux[ii])
        #plt.ylim(0,3e11)
        plt.title(ii + '\n' + str(database['Timestamp'].dropna().iloc[0]))
        plt.show()
    except:
        print(f'{ii} has some problem.')
# %%

This analysis is based on the scripts initially prepared by P. Belanger.

We briefly discussed with Stehane on the 1st Decemeber. Three main plots
- One overview of the cycle
- One on the luminosity, IP8 knobs, beta*
- One of the BB

### TODO
- check the optics files
- verify the possible longitudinal BU
- B2 losses when ramping down the octupoles
 

# Summary of MD7003 (2023 configuration)

Data are on /eos/project/l/lhc-lumimod/MD7003 (https://cernbox.cern.ch/s/bqcprspRvmwcecA).
In particular 

- the the parquet files are at /eos/project/l/lhc-lumimod/MD7003/FILLS/version_2.0

- the ADTOobsBox files are at /eos/project/l/lhc-lumimod/MD7003/ADTObsBox

- the effective cross sections pickle files (only for FILL 8470) are at /eos/project/l/lhc-lumimod/MD7003/Offline/8470 

I am using the python distribution you can `source /home/lumimod/MD7003/Executables/py_MD7003/bin/activate` as `lumimod@pcbe-abp-hpc002` (you can download this git and make your own distribution).
If you have not the `eos` access, ask one to guido.sterbini@cern.ch. 

#### PART 1
    
    FILL **8460** (START OF FILL - END OF FILL)
    2022-11-22 07:40:07.315 - 2022-11-22 12:06:58.332

    OP dumped

#### PART 2
    
    FILL **8461** (START OF FILL - END OF FILL)
    2022-11-22 12:06:58.332 - 2022-11-22 15:29:23.916

    OP dumped
 
#### Part 3

    FILL **8462** (START OF FILL - END OF FILL)
    2022-11-22 15:29:23.916 - 2022-11-22 18:28:16.849

    OP dumped

#### PART 4

    FILL **8469** (START OF FILL - END OF FILL)
    2022-11-23 13:27:43.414 - 2022-11-23 15:51:15.840

    ATLAS BCM Dumped (~10 s before collision). See https://cds.cern.ch/record/1158638/files/p264.pdf

    FILL **8470** (START OF FILL - END OF FILL)
    2022-11-23 15:51:15.840 - 2022-11-23 19:55:47.378
    
    OP dumped

### PARTS  1-3 summary
It has been a very successful MD where most of the planned activities could take place, namely 3 fills for:

a) FILL **8460**: validation down to beta*=60 cm at IP1/5 with
- off-momentum loss maps at 2 m after the rotation, 1.2 m before collision, 60 cm in collision (all run at low Q' between 3 and 5 to avoid sending the beam onto NL resonance during the RF trims, i.e. equivalent to perform the test with the QFB on)
- betatron loss maps at 60 cm (both beams), with TCT8 at 11.5 sigma (nominal values), then retracted to 12.0 sigma § 12.5 sigma (to indirectly check the margin with respect to the IR8 triplet aperture after the rotation)

b) FILL **8461**: first intensity ramp up step with 62 bunches @ 1.4e11 p/b (dumped by OP at 60 cm)

c) FILL **8462**: second step with 350 bunches @ 1.4e11 p/b (dumped by OP at 60 cm)

--> LMs in the first fill were validated, with the outstanding observation that the load on TCT1 and TCT5 are significantly higher than for the nominal cycle. This is interpreted as coming from the fact that, for the 2023 configuration,  the TCTs in IR1/5  are operated at (nearly) constant normalized gap during beta* leveling (to optimize the physics conditions of the FP experiments, CMS-PPS and AFP), versus a constant gap in mm for the 2022 cycle. Something to be followed up in terms of MP and and/or BLM thresholds for 2023.

--> The first intensity step run very smoothly. Collisions could be established and optimized in the 4 IPs (one 48b train colliding in IP1/5, 2 INDIVS colliding in IP2/8).
Before dumping at 60 cm, a MO polarity reversal test took place (from 300 A down to -590 A, by step) combined with a beam-based optimized tune shift (+0.003 along the diagonal overall). Note that similar tests were performed EoF in Run2. In the presence of BBLR in IR1 and IR5, some net improvement of lifetime was observed; although the optics is not telescopic at 60 cm (i.e. the MO efficiency is not optimal in terms of induced tune spread).

--> The second intensity step run very smoothly as well (filling scheme containing 7 48b trains with maximum beam-beam interactions in IR8), in particular the LHCb rotation in the presence of e-cloud. The rotation seems to be fully transparent for the beam lifetime and beam emittance (pending off-line analysis). The MO polarity test was repeated at 60 cm before dump, this time in one go. The  n.c. 12b were not unstable à premiere vue (no losses), but TBC, the lifetime recovers after the trim but did not improve (no improvement expected since no BBLR in IR1/5 for that filling scheme).

The ultimate goal of this program is to run with 3*48b trains at 1.8e11 p/b, with collisions in all 4 IPs to "validate" the  X-angle function (within +/- 10 murad) and the working point as a function of beta*, possibly using the octupole in the beam-beam dominated regime (1.8e11 p/b at 60 cm with a crossing angle of only 145 murad and PU=80-90 at IP1 and IP5)

**Stephane for the 2023 Machine Configuration Team**

### PART  4 summary

To complete the last part  of the program, namely running the full cycle down to beta*=60 cm with three 48b bunch trains @ 1.8e11 p/b,
3h00 were spontaneously offered by the LPC, which in the end turned ouf to be 5h00-6h00.

- The injector managed to deliver the requested beam (one 48b train per SPS injection with an average of about 1.75e11 p/b within an emittance of 1.8-1.9 micron).

- The first fill (**8469**) was ramped, "LHCb-rotated", "mini-squeezed" without any issue, but dumped by the ATLAS BCM a few second before reaching the collision.
Two similar dumps were observed during the heat-load MD in MD2 (at start of ramp, and during the ramp, with 36b trains @ 1.8e11 p/b).
This dump ("UFO-like") is not understood (nearly no losses on BLMs, or only very small in the area of TAN.L1).

- The second fill (**8470**) made it to collisions. To be noted that, a simple principle of precaution was applied by cleaning the abort gap just before ADJUST
(without claiming of course any possible connection with the former dump). Tunes were optimized at beta*=120 cm reaching a very decent beam lifetime in the range of 22-25h
(half X-angle of 135 murad). Beta* was then squeezed rather quickly to reach 60 cm (mu measured to be about 75-77), without any dramatic impact on the beam lifetime
despite a smaller than nominal crossing angle (145 murad vs. 160 murad) and higher bunch charge (about 1.65-1.7e11 p/b).
To be noted that some of the 12b n.c. of Beam2 became  unstable during the last beta* steps (and despite a further increase the Landau damping knob from -1.5 to -2.2).
In this configuration the bunch by bunch effective cross-section was measured in the range of 110-120 mb for Beam2, a bit worst for Beam1, with a clear BBLR dependent pattern.
The MO polarity reversal procedure (combined with a tune trim, as devised in FMD4) was applied, bringing these cross sections very close to their minimum of 80 mb.
A few MO cycle were tried out to increase the statistics. More detailed off-line analysis will follow.
The MD ended up with the manipulation of the new tilted limi-knobs of LHCb, which was apparently successful (pending off-line analysis of
their orthogonal impact on the z-position of  IP8 and on the lumi, respectively).

In summary the full program has been completed with a great success and a great pleasure !

**Stephane for the 2023 Machine Configuration Team**

The plots from Sofia are available at https://codimd.web.cern.ch/s/Z4zphIouL.

# Setup
1. Log into `lumimod@pcbe-abp-hpc002` and make a `kinit` (the latter is needed to access on `eos` volumes) 
2. Install the project and activate python environement
```bash
git clone ssh://git@gitlab.cern.ch:7999/abpcomputing/sandbox/MD7003.git
cd MD7003
bash make_it.sh
source Executables/py_MD7003/bin/activate
```
3. Edit paths in the `config.yaml` file. Please check also that the folders you are pointing at do exist (if not, create them).
4. Prepare `tmux` terminal with multiple tabs
```bash
# Tab 1, acquire data
# watch -n [seconds] python 000_onlineSpark.py
watch -n 240 python 000_onlineSpark.py

# Tab 2, monitor
tail -50f monitoring.log

# tab 3, after first download, choose calib_ts in config file and then
python 001_calibrateBCT.py  

# tab 4, plotting script
# watch -n [seconds] python 002_onlineSigmaEff.py
watch -n 10 python 002_plotWatcher.py
```

## Data
Data is stored under:
```bash
/eos/project/l/lhc-lumimod/MD7003/Online
```
It can be seen from the CERNBox browser at https://cernbox.cern.ch/s/bqcprspRvmwcecA.

## Logbook

## Check of the ObsBox
Verify from a TN machine (e.g. cs-ccr-dev1)  that the folder is updating
```
cd /nfs/cfc-sr4-adtobs2buf/obsbox/slow/B1H_Q7
find -cmin -1
```
 
